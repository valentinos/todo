/*
 * Javascript File
 * @author Valentinos Galanos <swg_lowraed@hotmail.com>
 * @version 1.0.0
 */
(function(win){
    var window = win;
    var ko = win.ko;
    var $ = win.jQuery;
    var TODO = win.TODO || {}; win.TODO = TODO;
    
    /*
     * Knockout Todo Application
     * author: Valentinos Galanos
     * base class: AppVModel
     * version: 1.0.0a
     */
    
    /* Runtime Objects */
    
    var BrowserStorage = function() {
        var self = this, local, session, storage;
        
        if(typeof(win.Storage) !== undefined) {
            local = win.localStorage;
            session = win.sessionStorage;
            storage = true;
        }
        
        self.save = function(name, data, s) {
            if(storage!==undefined) {
                var manager = (s===true) ? session : local;
                var json = win.JSON.stringify(data);
                manager.setItem(name, json);
                //console.debug(this,manager,json);
            }
        };
        
        self.load = function(name, s) {
            if(storage!==undefined) {
                var manager = (s===true) ? session : local;
                var json = manager[name];
                var o = win.JSON.parse((json!==undefined) ? json : '[]');
                //console.debug(this,manager,json,o);
                return o;
            }
        };
    };
    
    
    /* Runtime Models */
       
    var BaseModel = {
        extend : function(o){
            var base = BaseModel;
            var sub = o;
            sub.prototype = base;
            sub.prototype.constructor = sub;
            win.Object.defineProperty(sub.prototype, 'constructor',{
                enumerable: false,
                value: sub
            });
            return o;
        }
    };
    
    
    var TaskModel = BaseModel.extend(function(o){
            var self = this, 
                o = o || {};
            
            self['name'] = ko.observable(o.name || null);
            self['desc'] = ko.observable(o.desc || null);
            self['stat'] = ko.observable(o.stat || true);
            self['done'] = ko.observable(o.done || false);
            
            self['isDone'] = function(model,element){
                self.done(true);
                o.manager.save();
            };
            
            self['removeMe'] = function(model, ev) {
                if(o.manager === undefined) {throw 'Manager not defined';}
                var m = o.manager;
                var el = $(ev.target);
                el.parents('li').fadeOut(function(){
                    m.removeTask(model);
                });
                o.manager.save();
            };
    });
    
    var TaskManager = BaseModel.extend(function(o){
            var self = this, storage = new BrowserStorage(), 
                o = o || {};
            
            self['tasks'] = ko.observableArray(o.tasks || null);
            
            self['newTask'] = function(o) {
                var self = this;
                var task = new TaskModel($.extend({
                    manager: self,
                    stat: true,
                    done: false
                },o||{}));
                console.debug('New Task: ', task, o);
                win.NewTask = task;
                var idx = this.tasks().length;
                task['idx'] = ko.observable(idx);
                self.tasks.push(task);
            };
            self['addTask'] = function(task) {
                var idx = this.tasks().length;
                task['idx'] = ko.observable(idx);
                self.tasks.push(task);
            };
            self['removeTask'] = function(task) {
                var tasks = self.tasks;
                tasks.remove(task);
            };
            self['save'] = function() {
                setTimeout(function(){
                    var tasks = self.tasks(), d=[];
                    $.each(tasks, function(i){
                        var t = ko.toJS(tasks[i]);
                        d.push(t);
                    });
                    storage.save('tasks', d);
                }, 500);
            };
            self['load'] = function() {
                var tasks = storage.load('tasks');
                
                $.each(tasks, function(i){
                    var t = tasks[i];
                    self.newTask(t);
                });
            };
    });
    
    
    
    /* APPLICATION VIEW MODEL */
    var AppVModel = BaseModel.extend(function(config){
        var conf = config || {},
            self = this;
        self['config'] = conf;
        // Attributes
        self['name'] = ko.observable(conf.name || null);
        self['version'] = ko.observable(conf.version || null);
        
        // Components
        self['taskManager'] = new TaskManager();
        
        
        // Methods
        self['createTask'] = function(el) {
            var $el = $(el), t={};
            var data = $el.serializeArray();
            $.each(data,function(){
                t[this.name] = this.value;
            });
            self.taskManager.newTask(t);
            self.taskManager.save();
            $el.find('input').val('');
            $('.task-form').fadeOut();
        };
        self['newTask'] = function() {
            $('.task-form').fadeIn();
        };
    });
    
    
    
    
    /* ON DOCUMENT DOM READY */
    $(win.document).ready(function(){
        var app_config = {
            name: 'TODO List',
            version: '1.0.0'
        };
        win.APP = new AppVModel(app_config);
        ko.applyBindings(win.APP);
        
        win.APP.taskManager.load();
    });
    
    
    
    TODO['Runtime'] = {
        'TaskModel': TaskModel,
        'TaskManager': TaskManager
    };
    
})(this);
